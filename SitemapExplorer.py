from tkinter import Tk, Label, Entry, Button
from SitemapParser import SitemapParser;

class SitemapExplorer:
    root = None
    inputText = None

    def show(self):
        root = Tk()
        root.resizable(False, False)
        root.title('Website Explorer')

        Label(root, text='Url du sitemap.xml').grid(row=0, column=0)

        input = Entry(root, width=50)
        input.insert(0, 'https://www.aristys-web.com/sitemap.xml')
        input.grid(row=1, column=0)

        self.inputText = input

        Button(root, text='Scan', command=self.scan).grid(row=1, column=1)

        root.mainloop()

        self.root = root

    def scan(self):
        url=self.inputText.get()
        # print(url)
        # url = 'https://www.aristys-web.com/sitemap.xml'
        parser = SitemapParser()
        pages = parser.parse(url)
        i = 2
        for page in pages:
            # print(page['url'])
            # print(page['title'])
            # print(page['description'])
            e = Entry(self.root, width=50)
            e.insert(0, page['url'])
            e.grid(row=i, column=0)
            e = Entry(self.root, width=50)
            e.insert(0, page['title'])
            e.grid(row=i, column=1)
            e = Entry(self.root, width=50)
            e.insert(0, page['description'])
            e.grid(row=i, column=2)
            i += 1
