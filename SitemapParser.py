import http3
from bs4 import BeautifulSoup

class SitemapParser:
    def makeRequest(self, url):
        r = http3.get(url)
        if r.status_code == 200:
            return r.text

    def parseSitemap(self, content):
        soup = BeautifulSoup(content, "xml")
        urls = []
        for link in soup.find_all('loc'):
            urls.append(link.get_text())
        return urls

    def parseWebpage(self, content):
        soup = BeautifulSoup(content, "lxml")
        title = soup.head.title.text
        description = soup.find('meta', attrs={'name': 'description'})
        data = {'title': title, 'description': description['content']}
        return data;

    def parse(self, url):
        content = self.makeRequest(url)
        urls = self.parseSitemap(content)
        pages = []
        for url in urls:
            data = self.parseWebpage(self.makeRequest(url))
            data.update({'url': url})
            # print(data['url'])
            # print(data['title'])
            # print(data['description'])
            pages.append(data)
        return pages

# url = 'https://www.aristys-web.com/sitemap.xml'
# parser = SitemapParser()
# data = parser.parse(url)
